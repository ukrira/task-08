package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Const;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerException;
import java.io.IOException;

import static com.epam.rd.java.basic.task8.controller.DOMController.createDocument;
import static com.epam.rd.java.basic.task8.controller.DOMController.saveToXML;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private String currentElement;
    private Flowers flowerList;
    private Flower flower;
    private VisualParameters visualParameters;
    private AveLenFlower aveLenFlower;

    public final void parse(final boolean validate)
            throws ParserConfigurationException, SAXException, IOException {

        // obtain sax parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();

        // XML document contains namespaces
        factory.setNamespaceAware(true);

        // set validation
        if (validate) {
            factory.setFeature("http://xml.org/sax/features/validation", true);
            factory.setFeature("http://apache.org/xml/features/validation/schema",
                    true);
        }

        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }


    @Override
    public final void startElement(final String uri, final String localName,
                                   final String qName, final Attributes attributes) {
        currentElement = qName;
        if (Const.FLOWERS.equals(currentElement)) {
            flowerList = new Flowers();
            flowerList.setXmlns("http://www.nure.ua");
            flowerList.setXmlns_xsi("http://www.w3.org/2001/XMLSchema-instance");
            flowerList.setSchema(attributes.getValue(Const.SCHEMA_LOCATION));
        }
        if (Const.FLOWER.equals(currentElement)) {
            flower = new Flower();
        }

        if (Const.VISUAL_PARAMETERS.equals(currentElement)) {
            visualParameters = new VisualParameters();
        }
        if (Const.AVE_LEN_FLOWER.equals(currentElement)) {
            String measure = attributes.getValue(Const.MEASURE);
            aveLenFlower = new AveLenFlower(measure);//length did not put
        }
    }

    @Override
    public final void characters(final char[] ch, final int start, final int length) {
        String elementText = new String(ch, start, length).trim();
        // return if content is empty
        if (Const.NAME.equals(currentElement)) {
            flower.setName(elementText);
        }
        if (Const.SOIL.equals(currentElement)) {
            flower.setSoil(Soil.valueOf(elementText));
        }
        if (Const.ORIGIN.equals(currentElement)) {
            flower.setOrigin(elementText);
        }
        if (Const.STEM_COLOUR.equals(currentElement)) {
            visualParameters.setStemColour(elementText);
        }
        if (Const.LEAF_COLOUR.equals(currentElement)) {
            visualParameters.setLeafColour(elementText);
        }
        if (Const.AVE_LEN_FLOWER.equals(currentElement)) {
            aveLenFlower.setAvaLenFlower(Integer.valueOf(elementText));
        }
        if (Const.MULTIPLYING.equals(currentElement)) {
            flower.setMultiplying(Multiplying.valueOf(elementText));
        }
    }

    @Override
    public final void endElement(final String uri, final String localName, final String qName) {
        if (Const.FLOWER.equals(localName)) {
            flowerList.addFlower(flower);
            flower = null;
        }
        if (Const.VISUAL_PARAMETERS.equals(localName)) {
            flower.setVisualParameters(visualParameters);
            visualParameters = null;
        }
        if (Const.AVE_LEN_FLOWER.equals(localName)) {
            visualParameters.setAveLenFlower(aveLenFlower);
            aveLenFlower = null;
        }
    }

    public Flowers getFlowerList() {
        return flowerList;
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, TransformerException {
        SAXController saxController = new SAXController("input.xml");//new SAXController(xmlFileName)
        try {
            saxController.parse(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Flowers flowerList = saxController.getFlowerList();
        System.out.println(flowerList);

        String outputXmlFile = "output.sax.xml";
        Document myDoc = createDocument(flowerList);
        saveToXML(myDoc, outputXmlFile);
    }

}
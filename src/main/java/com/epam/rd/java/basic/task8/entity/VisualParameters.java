package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private AveLenFlower aveLenFlower;

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VisualParameters)) return false;
        VisualParameters that = (VisualParameters) o;
        return Objects.equals(getStemColour(), that.getStemColour()) && Objects.equals(getLeafColour(), that.getLeafColour());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStemColour(), getLeafColour());
    }
}

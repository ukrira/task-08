package com.epam.rd.java.basic.task8.entity;

public class AveLenFlower {
    private String measure;
    private Integer avaLenFlower;

    public AveLenFlower(String measure, Integer avaLenFlower) {
        this.measure = measure;
        this.avaLenFlower = avaLenFlower;
    }

    public AveLenFlower(String measure) {
        this.measure = measure;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public Integer getAvaLenFlower() {
        return avaLenFlower;
    }

    public void setAvaLenFlower(Integer avaLenFlower) {
        this.avaLenFlower = avaLenFlower;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "measure='" + measure + '\'' +
                ", avaLenFlower=" + avaLenFlower +
                '}';
    }
}

package com.epam.rd.java.basic.task8.entity;

public enum Soil {
    подзолистая("подзолистая"),
    грунтовая("грунтовая"),
    дерново_подзолистая("дерново_подзолистая");

    private final String value;


    Soil(String value) {
        this.value = value;
    }

    private final String value() {
        return value;
    }

    public static Soil fromValue(final String v) {
        for (Soil c : Soil.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

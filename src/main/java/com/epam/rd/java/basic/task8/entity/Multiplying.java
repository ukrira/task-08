package com.epam.rd.java.basic.task8.entity;

public enum Multiplying {
    листья("листья"),
    черенки("черенки"),
    семена("семена");


    private final String value;


    Multiplying(String value) {
        this.value = value;
    }

    private final String value() {
        return value;
    }

    public static Multiplying fromValue(final String v) {
        for (Multiplying m : Multiplying.values()) {
            if (m.value.equals(v)) {
                return m;
            }
        }
        throw new IllegalArgumentException(v);
    }

}


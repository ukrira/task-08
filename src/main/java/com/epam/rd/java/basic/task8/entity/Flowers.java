package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private String xmlns;
    private String xmlns_xsi;
    private String schema;

    private final List<Flower> flowerList;

    public Flowers() {
        flowerList = new ArrayList<>();
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getXmlns_xsi() {
        return xmlns_xsi;
    }

    public void setXmlns_xsi(String xmlns_xsi) {
        this.xmlns_xsi = xmlns_xsi;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "xmlns='" + xmlns + '\'' +
                ", xmlns_xsi='" + xmlns_xsi + '\'' +
                ", schema='" + schema + '\'' +
                ", flowerList=" + flowerList +
                '}';
    }

    public void addFlower(Flower flower){
        flowerList.add(flower);
    }
}

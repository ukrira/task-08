package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Const;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;


public class DOMController {

    private String xmlFileName = "input.xml";
    private Flowers flowerList;


    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        Document document = getDocument();
        flowerList = createFlowerlist(document);
        Flower flower;
        VisualParameters visualParameters;
        NodeList flowerNode = document.getFirstChild().getChildNodes();
        Node visualParamNode;

        for (int i = 0; i < flowerNode.getLength(); i++) {
            if (flowerNode.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            flower = new Flower();
            flowerList.addFlower(flower);
            NodeList flowerChildsNodes = flowerNode.item(i).getChildNodes();

            for (int j = 0; j < flowerChildsNodes.getLength(); j++) {
                if (flowerChildsNodes.item(j).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                switch (flowerChildsNodes.item(j).getNodeName()) {
                    case Const.NAME: {
                        flower.setName(flowerChildsNodes.item(j).getTextContent());
                        break;
                    }
                    case Const.SOIL: {
                        flower.setSoil(Soil.valueOf((flowerChildsNodes.item(j).getTextContent())));
                        break;

                    }
                    case Const.ORIGIN: {
                        flower.setOrigin(flowerChildsNodes.item(j).getTextContent());
                        break;
                    }
                    case Const.VISUAL_PARAMETERS: {
                        visualParamNode = flowerChildsNodes.item(j);
                        flower.setVisualParameters(getVisParam(visualParamNode));
                        break;
                    }
                    case Const.MULTIPLYING: {
                        flower.setMultiplying(Multiplying.valueOf(flowerChildsNodes.item(j).getTextContent()));
                        break;
                    }
                }
            }
        }
    }


    private static VisualParameters getVisParam(Node node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }
        NodeList visualParamChildNodes = node.getChildNodes();
        VisualParameters visualParameters = new VisualParameters();
        for (int j = 0; j < visualParamChildNodes.getLength(); j++) {
            if (visualParamChildNodes.item(j).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            switch (visualParamChildNodes.item(j).getNodeName()) {
                case Const.STEM_COLOUR: {
                    visualParameters.setStemColour(visualParamChildNodes.item(j).getTextContent());
                    break;
                }
                case Const.LEAF_COLOUR: {
                    visualParameters.setLeafColour(visualParamChildNodes.item(j).getTextContent());
                    break;
                }
                case Const.AVE_LEN_FLOWER: {
                    Integer len = Integer.valueOf(visualParamChildNodes.item(j).getTextContent());
                    NamedNodeMap attributes = visualParamChildNodes.item(j).getAttributes();
                    String measure = attributes.getNamedItem(Const.MEASURE).getNodeValue();
                    AveLenFlower a = new AveLenFlower(measure, len);
                    visualParameters.setAveLenFlower(a);
                    break;
                }
            }
        }
        return visualParameters;
    }

    private static Document getDocument() {
        // Получение фабрики, чтобы после получить билдер документов.
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        // Запарсили XML, создав структуру Document. Теперь у нас есть доступ ко всем элементам
        Document document = null;
        try {
            if (db != null) {
                document = db.parse(new File("input.xml"));
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    public static Document createDocument(Flowers flowerList) throws ParserConfigurationException {
        // obtain DOM parser
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // XML document contains namespaces
        dbf.setNamespaceAware(true);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();

        // create root element
        Element rootElement = document.createElement(Const.FLOWERS);
        rootElement.setAttribute(Const.XMLNS, flowerList.getXmlns());
        rootElement.setAttribute(Const.XMLNS_XSI, flowerList.getXmlns_xsi());
        rootElement.setAttribute(Const.SCHEMA_LOCATION, flowerList.getSchema());

        // add root element
        document.appendChild(rootElement);
        for (Flower flower : flowerList.getFlowerList()) {
            //add Flower to flowers
            Element flowerElement = document.createElement(Const.FLOWER);
            rootElement.appendChild(flowerElement);
            //add name to flower
            Element nameElement = document.createElement(Const.NAME);
            nameElement.setTextContent(flower.getName());
            flowerElement.appendChild(nameElement);
            //add soil to flower
            Element soilElement = document.createElement(Const.SOIL);
            soilElement.setTextContent(String.valueOf(flower.getSoil()));
            flowerElement.appendChild(soilElement);
            //add origin to flower
            Element originElement = document.createElement(Const.ORIGIN);
            originElement.setTextContent(flower.getOrigin());
            flowerElement.appendChild(originElement);
            //create visual param
            Element visualParamElement = document.createElement(Const.VISUAL_PARAMETERS);
            // add stemColour to visual param
            Element stemColourElement = document.createElement(Const.STEM_COLOUR);
            stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
            visualParamElement.appendChild(stemColourElement);
            //add leafColour to visual param
            Element leafColourElement = document.createElement(Const.LEAF_COLOUR);
            leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
            visualParamElement.appendChild(leafColourElement);
            //add aveLenFlower to visual param
            Element aveLenFlowerElement = document.createElement(Const.AVE_LEN_FLOWER);
            aveLenFlowerElement.setAttribute(Const.MEASURE, flower.getVisualParameters().getAveLenFlower().getMeasure());
            aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getAvaLenFlower().toString());
            visualParamElement.appendChild(aveLenFlowerElement);
            //add visual param to flower
            flowerElement.appendChild(visualParamElement);
            //add multiplying to flower
            Element multiplyingElement = document.createElement(Const.MULTIPLYING);
            multiplyingElement.setTextContent(String.valueOf(flower.getMultiplying()));
            flowerElement.appendChild(multiplyingElement);

        }
        return document;
    }

    public static void saveToXML(Document document, String xmlFileName) throws TransformerException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        // set up transformation
        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");

        // run transformation
        t.transform(new DOMSource(document), result);
    }

    public static void main(String[] args) throws ParserConfigurationException, TransformerException {
        DOMController domController = new DOMController("input.xml");
        Flowers flowerList = domController.flowerList;
        System.out.println("DOM LIST " + flowerList);
        Document myDoc = createDocument(flowerList);
        saveToXML(myDoc, "output.dom.xml");


    }

    public Flowers createFlowerlist(Document document) {
        // create container
        flowerList = new Flowers();
        //set root param
        Element root = document.getDocumentElement();
        String nodeRoot = document.getDocumentElement().getNodeName();
        String xmlns = document.getDocumentElement().getAttribute(Const.XMLNS);
        flowerList.setXmlns(xmlns);
        String xmlns_xsi = document.getDocumentElement().getAttribute(Const.XMLNS_XSI);
        flowerList.setXmlns_xsi(xmlns_xsi);
        String xsi_schemaLocation = document.getDocumentElement().getAttribute(Const.SCHEMA_LOCATION);
        flowerList.setSchema(xsi_schemaLocation);
        return flowerList;
    }

    public Flowers getFlowerList() {
        return flowerList;
    }

}




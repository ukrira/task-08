package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.uitl.Sorter;
import org.w3c.dom.Document;

import java.io.IOException;

import static com.epam.rd.java.basic.task8.controller.DOMController.createDocument;
import static com.epam.rd.java.basic.task8.controller.DOMController.saveToXML;

public class Main {

    public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        Flowers flowerList = domController.getFlowerList();
        System.out.println(flowerList);
        // sort (case 1)
        Sorter.sortByName(flowerList);
        // save
        String outputXmlFile = "output.dom.xml";

        Document myDoc = createDocument(flowerList);
        saveToXML(myDoc, outputXmlFile);
        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        try {
            saxController.parse(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        flowerList = saxController.getFlowerList();
        System.out.println(flowerList);
        // sort  (case 2)
       Sorter.sortByOrigin(flowerList);

        // save
        outputXmlFile = "output.sax.xml";
        myDoc = createDocument(flowerList);
        saveToXML(myDoc, outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parse();
        flowerList = saxController.getFlowerList();
        // PLACE YOUR CODE HERE

        // sort  (case 3)
        Sorter.SortByLeafColour(flowerList);

        // save
        outputXmlFile = "output.stax.xml";
        myDoc = createDocument(flowerList);
        DOMController.saveToXML(myDoc, outputXmlFile);
        System.out.println("Output ==> " + outputXmlFile);
    }

}

package com.epam.rd.java.basic.task8.uitl;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Sorter {
    public static Comparator<Flower> sortByName = Comparator.comparing(Flower::getName);

    public static Comparator<Flower> sortByOrigin = Comparator.comparing(Flower::getOrigin);

    public static Comparator<Flower>sortByLeafColour =
            Comparator.comparing((Flower o) -> o.getVisualParameters().getLeafColour());

    public static void sortByName(Flowers flowerslist){
        flowerslist.getFlowerList().sort(sortByName);
    }
    public static void sortByOrigin(Flowers flowerslist){
        flowerslist.getFlowerList().sort(sortByOrigin);
    }
    public static void SortByLeafColour(Flowers flowerlist){
        flowerlist.getFlowerList().sort(sortByLeafColour);
    }


}

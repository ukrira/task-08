package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Const;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;
    private String currentElement;
    private Flowers flowerList;
    private Flower flower;
    private VisualParameters visualParameters;
    private AveLenFlower aveLenFlower;


    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public final void parse() throws XMLStreamException {

        // current element name holder
        String currentElement = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            // skip any empty content
            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }
            // handler for start tags
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                if ("flowers".equals(currentElement)) {
                    Attribute xmlns = startElement.getAttributeByName(new QName(Const.XMLNS));
                    Attribute xsi = startElement.getAttributeByName(new QName(Const.XMLNS_XSI));
                    Attribute schema = startElement.getAttributeByName(new QName(Const.SCHEMA_LOCATION));
                    flowerList = new Flowers();
                    flowerList.setXmlns("http://www.nure.ua");
                    flowerList.setXmlns_xsi("http://www.w3.org/2001/XMLSchema-instance");
                    flowerList.setSchema(String.valueOf(schema));
                }
                if (Const.FLOWER.equals(currentElement)) {
                    flower = new Flower();
                }
                if (Const.VISUAL_PARAMETERS.equals(currentElement)) {
                    visualParameters = new VisualParameters();
                }
                if (Const.AVE_LEN_FLOWER.equals(currentElement)) {
                    Attribute attribute = startElement.getAttributeByName(new QName(Const.MEASURE));
                    if (attribute != null) {
                        aveLenFlower = new AveLenFlower(attribute.getValue());
                        visualParameters.setAveLenFlower(aveLenFlower);

                    }
                }
            }
            // handler for contents
            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                String elementText = characters.getData();
                if (Const.NAME.equals(currentElement)) {
                    flower.setName(elementText);
                }
                if (Const.SOIL.equals(currentElement)) {
                    flower.setSoil(Soil.valueOf(elementText));
                }
                if (Const.ORIGIN.equals(currentElement)) {
                    flower.setOrigin(elementText);
                }
                if (Const.STEM_COLOUR.equals(currentElement)) {
                    visualParameters.setStemColour(elementText);
                }
                if (Const.LEAF_COLOUR.equals(currentElement)) {
                    visualParameters.setLeafColour(elementText);
                }
                if (Const.AVE_LEN_FLOWER.equals(currentElement)) {
                    aveLenFlower.setAvaLenFlower(Integer.valueOf(elementText));
                }
                if (Const.MULTIPLYING.equals(currentElement)) {
                    flower.setMultiplying(Multiplying.valueOf(elementText));
                }
            }
            // handler for end tags
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();

                if (Const.FLOWERS.equals(localName)) {
                    flowerList.addFlower(flower);
                    flower = null;
                }
                if (Const.VISUAL_PARAMETERS.equals(localName)) {
                    flower.setVisualParameters(visualParameters);
                }
                if (Const.AVE_LEN_FLOWER.equals(localName)) {
                    visualParameters.setAveLenFlower(aveLenFlower);
                }
            }
        }
        reader.close();
    }
    public Flowers getFlowerList(){
        return flowerList;
    }
}